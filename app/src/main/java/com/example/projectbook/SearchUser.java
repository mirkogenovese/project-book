package com.example.projectbook;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbook.adapters.RecyclerAdapterUser;
import com.example.projectbook.objects.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;

public class SearchUser extends AppCompatActivity {

    private static final String TAG = "SearchUser";
    ArrayList<User> listUsers;
    RecyclerAdapterUser adapter;
    final LinkedList<ValueEventListener> usersListListeners = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_recycler_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initializeUsersList();
    }

    public void initializeUsersList() {

        RecyclerView usersRecycleView = findViewById(R.id.recycler_view_list);
        usersRecycleView.setLayoutManager(new LinearLayoutManager(this));

        listUsers = new ArrayList<>();
        adapter = new RecyclerAdapterUser(listUsers);
        usersRecycleView.setAdapter(adapter);

        usersListListeners.add(FirebaseDatabase.getInstance().getReference("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        }));

    }

    private void showData(DataSnapshot dataSnapshot){
        listUsers.clear();
        for(DataSnapshot ds : dataSnapshot.getChildren()){

            User user = new User();
            user.key = ds.getKey();

            Object username = ds.child("username").getValue();
            user.username = username != null ? username.toString() : "";

            Object type = ds.child("type").getValue();
            user.type = type != null ? type.toString() : "";

            Object image = ds.child("image").getValue();
            user.image = image != null ? image.toString() : null;

            listUsers.add(user);
            adapter.notifyDataSetChanged();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        for(ValueEventListener l : usersListListeners)
            FirebaseDatabase.getInstance().getReference("Users").removeEventListener(l);
    }
}