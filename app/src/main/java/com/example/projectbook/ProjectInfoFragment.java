package com.example.projectbook;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;

import static android.app.Activity.RESULT_OK;

public class ProjectInfoFragment extends Fragment {

    TextView label_location;
    ImageView imageView;
    View inflated;

    public ProjectInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        inflated = inflater.inflate(R.layout.fragment_project_info, container, false);

        if(!ProjectView.project.key.equals("")) {
            ((TextView) inflated.findViewById(R.id.tx_project_type)).setText (ProjectView.project.type);
            ((TextView) inflated.findViewById(R.id.tx_project_description)).setText (ProjectView.project.description);

            ((TextView) inflated.findViewById(R.id.label_project_type)).setText(ProjectView.project.type);
            ((TextView) inflated.findViewById(R.id.label_project_description)).setText (ProjectView.project.description);

            ((TextView) inflated.findViewById(R.id.label_location)).setText (ProjectView.project.locationString);

            inflated.findViewById(R.id.label_project_type).setVisibility(View.VISIBLE);
            inflated.findViewById(R.id.label_project_description).setVisibility(View.VISIBLE);

            inflated.findViewById(R.id.tx_project_type).setVisibility(View.GONE);
            inflated.findViewById(R.id.tx_project_description).setVisibility(View.GONE);
        }
        else {
            inflated.findViewById(R.id.label_project_type).setVisibility(View.GONE);
            inflated.findViewById(R.id.label_project_description).setVisibility(View.GONE);

            inflated.findViewById(R.id.tx_project_type).setVisibility(View.VISIBLE);
            inflated.findViewById(R.id.tx_project_description).setVisibility(View.VISIBLE);

            ((TextView) inflated.findViewById(R.id.label_location)).setText (R.string.project_hint_location);

            inflated.findViewById(R.id.label_location).setEnabled(true);
        }
        if(ProjectView.project.image == null) {
            if(ProjectView.newImage != null)
                ((ImageView)inflated.findViewById(R.id.image_project)).setImageBitmap(ProjectView.newImage);
            else
                ((ImageView)inflated.findViewById(R.id.image_project)).setImageResource(R.drawable.project_image);
        }
        else {
            Glide.with(inflated.getContext())
                    .load(Uri.parse(ProjectView.project.image))
                    .into(((ImageView)inflated.findViewById(R.id.image_project)));
        }

        ((TextView) inflated.findViewById(R.id.label_createOn)).setText (ProjectView.project.createOn);

        label_location = (TextView)inflated.findViewById(R.id.label_location);
        label_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ProjectView.project.getLocation() == null)
                    if(!ProjectView.isEditMode)
                        return;

                Intent intent = (new Intent(inflated.getContext(), ProjectLocationActivity.class));
                if(ProjectView.project.getLocation() != null)
                    intent.putExtra("location", ProjectView.project.getLocation());
                if (ProjectView.isEditMode)
                    intent.putExtra("command", "selectLocation");
                else
                    intent.putExtra("command", "showLocation");

                startActivityForResult(intent, 1);
            }
        });
        imageView = inflated.findViewById(R.id.image_project);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });
        return inflated;
    }

    private void openGallery() {
        if(ProjectView.isEditMode) {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(gallery, 2);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != RESULT_OK || data == null) return;

        if (requestCode == 1) {
            LatLng location = data.getParcelableExtra("location");
            String locationString = data.getStringExtra("locationString");
            label_location.setText(locationString);

            ProjectView.tmpLocation = location;
        }

        if (requestCode == 2) {
            Uri imageUri = data.getData();
            imageView.setImageURI(imageUri);
            imageView.setDrawingCacheEnabled(true);
            imageView.buildDrawingCache();
            ProjectView.newImage = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        }
    }
}