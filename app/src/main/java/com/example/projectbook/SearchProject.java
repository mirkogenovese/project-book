package com.example.projectbook;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbook.adapters.RecyclerAdapterProject;
import com.example.projectbook.objects.Project;
import com.example.projectbook.utilities.Utilities;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;

public class SearchProject extends AppCompatActivity {

    private static final String TAG = "SearchProject";
    public static ArrayList<Project> listProjects;
    RecyclerAdapterProject adapter;
    final LinkedList<ValueEventListener> projectsListListeners = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_recycler_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
           public boolean onMenuItemClick(MenuItem item) {
               if(item.getItemId() == R.id.menu_map) {
                   openProjectsMap();
                   return true;
               }
               return false;
           }
       });

        initializeProjectsList();
    }

    public void openProjectsMap() {
        Intent intent = (new Intent(getApplicationContext(), ProjectLocationActivity.class));
        intent.putExtra("command", "selectProject");
        startActivityForResult(intent, 1);
    }

    public void initializeProjectsList() {

        RecyclerView projectRecycleView = findViewById(R.id.recycler_view_list);
        projectRecycleView.setLayoutManager(new LinearLayoutManager(this));

        listProjects = new ArrayList<>();
        adapter = new RecyclerAdapterProject(listProjects);
        projectRecycleView.setAdapter(adapter);

        projectsListListeners.add(FirebaseDatabase.getInstance().getReference("Projects").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        }));

    }

    private void showData(DataSnapshot dataSnapshot){
        listProjects.clear();
        for(DataSnapshot ds : dataSnapshot.getChildren()){

            Project project = new Project();
            project.key = ds.getKey();

            Object name = ds.child("name").getValue();
            project.name = name != null ? name.toString() : "";

            Object type = ds.child("type").getValue();
            project.type = type != null ? type.toString() : "";

            Object chat = ds.child("chat").getValue();
            project.chat = chat != null ? chat.toString() : "";

            Object ownerKey = ds.child("ownerKey").getValue();
            project.ownerKey = ownerKey != null ? ownerKey.toString() : "";

            Object image = ds.child("image").getValue();
            project.image = image != null ? image.toString() : null;

            Double latitude = (Double) ds.child("location").child("latitude").getValue();
            Double longitude = (Double) ds.child("location").child("longitude").getValue();
            if(latitude != null && longitude != null) {
                project.latitude = latitude;
                project.longitude = longitude;
                project.locationString = Utilities.getAddress(project.getLocation(), getApplicationContext());
            }

            Object description = ds.child("description").getValue();
            project.description = description != null ? description.toString() : "";

            Object createOn = ds.child("createOn").getValue();
            project.createOn = createOn != null ? createOn.toString() : "";

            for(DataSnapshot part : ds.child("Participants").getChildren()) {
                project.participants.add(part.getKey());
            }

            listProjects.remove(project);
            listProjects.add(project);
            adapter.notifyDataSetChanged();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_project_search, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        for(ValueEventListener l : projectsListListeners)
            FirebaseDatabase.getInstance().getReference("Projects").removeEventListener(l);
    }
}