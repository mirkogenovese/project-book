package com.example.projectbook;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.example.projectbook.adapters.SectionsPagerAdapter;
import com.example.projectbook.objects.ChatItem;
import com.example.projectbook.objects.Project;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class ProjectView extends AppCompatActivity {

    public static Project project;
    public static boolean isEditMode = false;
    private boolean isParticipant = false;
    public static LatLng tmpLocation;
    public static Bitmap newImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEditMode)
                    discardEditDialog();
                else
                    finish();
            }
        });

        setClickListener(toolbar);

        Project argProject = (Project) getIntent().getSerializableExtra("project");
        if(argProject == null) {
            project = new Project();
            project.name = getIntent().getStringExtra("name");
            isEditMode = true;
        } else {
            project = argProject;

            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser mFirebaseUser = mAuth.getCurrentUser();

            if(project.participants.contains(mFirebaseUser.getUid())) {
                isParticipant = true;
            }
        }

        getSupportActionBar().setTitle(project.name);
        initializeTabs();
    }

    public void initializeTabs() {
        TabLayout tabs = findViewById(R.id.tabs);
        ViewPager viewPager = findViewById(R.id.view_pager);

        tabs.removeAllTabs();
        tabs.addTab(tabs.newTab());
        tabs.addTab(tabs.newTab());
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(), tabs.getTabCount());
        viewPager.setAdapter(sectionsPagerAdapter);

        tabs.setupWithViewPager(viewPager);
    }

    private void discardEditDialog() {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(getString(R.string.project_dialog_title));
        alert.setMessage(getString(R.string.project_dialog_message));
        alert.setButton(Dialog.BUTTON_POSITIVE,getString(R.string.project_dialog_leave),new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editModeSwitch(false);
                finish();
            }
        });
        alert.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.project_dialog_stay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alert.show();
    }

    private void setClickListener(Toolbar toolbar) {
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_save:
                        updateProject();
                        editModeSwitch(false);
                        return true;
                    case R.id.menu_chat:
                        openGroupChat();
                        return true;
                    case R.id.menu_edit:
                        editModeSwitch(true);
                        return true;
                    case R.id.menu_join:
                        joinProject();
                        return true;
                    case R.id.menu_leave:
                        leaveProject();
                        return true;
                    case R.id.menu_rename:
                        renameProject();
                        return true;
                    case R.id.menu_delete:
                        deleteProject();
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_project_view, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        if (isEditMode) {
            menu.findItem(R.id.menu_save).setEnabled(true);
            menu.findItem(R.id.menu_chat).setEnabled(false);
            menu.findItem(R.id.menu_edit).setEnabled(false);
            menu.findItem(R.id.menu_join).setEnabled(false);
            menu.findItem(R.id.menu_leave).setEnabled(false);
            menu.findItem(R.id.menu_rename).setEnabled(false);
            menu.findItem(R.id.menu_delete).setEnabled(false);
        }
        else if (isParticipant) {
            menu.findItem(R.id.menu_save).setEnabled(false).setVisible(false);
            menu.findItem(R.id.menu_chat).setEnabled(true);
            menu.findItem(R.id.menu_edit).setEnabled(true);
            menu.findItem(R.id.menu_join).setEnabled(false);
            menu.findItem(R.id.menu_leave).setEnabled(true);
            menu.findItem(R.id.menu_rename).setEnabled(true);
            menu.findItem(R.id.menu_delete).setEnabled(true);
        }
        else {
            menu.findItem(R.id.menu_save).setEnabled(false).setVisible(false);
            menu.findItem(R.id.menu_chat).setEnabled(false);
            menu.findItem(R.id.menu_edit).setEnabled(false);
            menu.findItem(R.id.menu_join).setEnabled(true);
            menu.findItem(R.id.menu_leave).setEnabled(false);
            menu.findItem(R.id.menu_rename).setEnabled(false);
            menu.findItem(R.id.menu_delete).setEnabled(false);
        }
        return true;
    }

    public void updateProject() {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Projects");

        project.type = ((TextView)findViewById(R.id.tx_project_type)).getText().toString();
        project.description = ((TextView)findViewById(R.id.tx_project_description)).getText().toString();

        if(tmpLocation != null) {
            project.setLocation(tmpLocation);
            project.locationString = ((TextView)findViewById(R.id.label_location)).getText().toString();
        }

        if(project.key.isEmpty()) {
            project.key = dbRef.push().getKey();
            dbRef.child(project.key).child("ownerKey").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
            joinProject();
        }

        dbRef.child(project.key).child("name").setValue(project.name);
        dbRef.child(project.key).child("type").setValue(project.type);
        dbRef.child(project.key).child("description").setValue(project.description);
        dbRef.child(project.key).child("createOn").setValue(project.createOn);
        dbRef.child(project.key).child("location").child("latitude").setValue(project.latitude);
        dbRef.child(project.key).child("location").child("longitude").setValue(project.longitude);

        if(newImage != null) {
            uploadImage();
        }

        Toast.makeText(this, R.string.toast_project_updated, Toast.LENGTH_LONG).show();
        initializeTabs();
    }

    public void uploadImage() {
        StorageReference mountainsRef = FirebaseStorage.getInstance().getReference("images/project/" + project.key + "jpg");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                if (taskSnapshot.getMetadata() != null) {
                    if (taskSnapshot.getMetadata().getReference() != null) {
                        Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                        result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String imageUrl = uri.toString();
                                Log.w("ProjectView", "Upload OK, URL: " + imageUrl);

                                DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Projects");
                                dbRef.child(project.key).child("image").setValue(imageUrl);
                            }
                        });
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.w("ProjectView", "Upload failed");
            }
        });
    }

    public void editModeSwitch(boolean value) {
        if(value) {
            findViewById(R.id.label_project_type).setVisibility(View.GONE);
            findViewById(R.id.label_project_description).setVisibility(View.GONE);

            findViewById(R.id.tx_project_type).setVisibility(View.VISIBLE);
            findViewById(R.id.tx_project_description).setVisibility(View.VISIBLE);

            if(project.locationString == null || project.locationString.isEmpty())
                ((TextView)findViewById(R.id.label_location)).setText(R.string.project_hint_location);
        } else {
            ((TextView)findViewById(R.id.label_project_type)).setText(project.type);
            ((TextView)findViewById(R.id.label_project_description)).setText(project.description);

            findViewById(R.id.label_project_type).setVisibility(View.VISIBLE);
            findViewById(R.id.label_project_description).setVisibility(View.VISIBLE);

            findViewById(R.id.tx_project_type).setVisibility(View.GONE);
            findViewById(R.id.tx_project_description).setVisibility(View.GONE);

            if(project.locationString == null || project.locationString.equals(""))
                ((TextView)findViewById(R.id.label_location)).setText("");

            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = findViewById(R.id.tx_project_type).getRootView();
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        }
        isEditMode = value;
        invalidateOptionsMenu();
    }

    public void openGroupChat() {

        if(project.chat.equals("")) {
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Chats");
            ChatItem chat = new ChatItem();
            chat.groupKey = project.key;
            String key = dbRef.push().getKey();

            FirebaseDatabase.getInstance().getReference("/Chats").child(key).setValue(chat);
            FirebaseDatabase.getInstance().getReference("/Projects").child(project.key).child("chat").setValue(key);

            project.chat = key;
        }

        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Users");
        dbRef.child(mFirebaseUser.getUid()).child("Chats").child(project.chat).setValue(0);

        Intent intent = new Intent(this, ConversationsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void joinProject() {

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
        String key = dbRef.child("Users").child(project.ownerKey).child("Notifications").push().getKey();
        dbRef = dbRef.child("Users").child(project.ownerKey).child("Notifications").child(key);
        dbRef.child("userKey").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
        dbRef.child("projectKey").setValue(project.key);
        dbRef.child("type").setValue(1);

        Toast.makeText(this, R.string.toast_join_request, Toast.LENGTH_LONG).show();
    }

    public void leaveProject() {
        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseDatabase.getInstance().getReference("/Projects").child(project.key).child("Participants").child(mFirebaseUser.getUid()).removeValue();
        FirebaseDatabase.getInstance().getReference("/Users").child(mFirebaseUser.getUid()).child("Projects").child(project.key).removeValue();
        isParticipant = false;
        finish();
    }

    public void renameProject() {
        if(!FirebaseAuth.getInstance().getCurrentUser().getUid().equals(project.ownerKey)) {
            Toast.makeText(this, R.string.toast_project_rename_failed, Toast.LENGTH_LONG).show();
            return;
        }
        showInputNameDialog();
    }

    private void showInputNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.project_dialog_name);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

        builder.setPositiveButton(R.string.project_dialog_name_apply, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(input.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.project_dialog_invalid_name, Toast.LENGTH_LONG).show();
                    return;
                }
                DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Projects");

                dbRef.child(project.key).child("name").setValue(input.getText().toString());
                getSupportActionBar().setTitle(input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.project_dialog_name_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void deleteProject() {
        if(!FirebaseAuth.getInstance().getCurrentUser().getUid().equals(project.ownerKey)) {
            Toast.makeText(this, R.string.toast_project_delete_failed, Toast.LENGTH_LONG).show();
            return;
        }
        showDeleteDialog();
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.project_dialog_delete);

        final TextView message = new TextView(this);
        message.setText(R.string.project_dialog_delete_message);
        builder.setView(message);

        builder.setPositiveButton(R.string.project_dialog_delete_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Projects");
                for(String key : project.participants) {
                    FirebaseDatabase.getInstance().getReference("/Users").child(key).child("Projects").child(project.key).removeValue();
                }
                dbRef.child(project.key).removeValue();
                finish();
            }
        });
        builder.setNegativeButton(R.string.project_dialog_delete_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

}