package com.example.projectbook;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class ProfileEditor extends AppCompatActivity {

    boolean isEditMode = false;
    String uid;
    EditText txUsername;
    EditText txType;
    EditText txEmail;
    EditText txPassword;
    String oldUsername;
    String oldType;
    String oldEmail;
    ImageView imageView;
    private static final int PICK_IMAGE = 100;
    Bitmap newImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_editor);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEditMode)
                    discardEditDialog();
                else
                    finish();
            }
        });

        findViewById(R.id.bt_profile_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editModeSwitch(true);
            }
        });
        findViewById(R.id.bt_profile_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
                editModeSwitch(false);
            }
        });

        imageView = findViewById(R.id.image_profile);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });

        txUsername = findViewById(R.id.tx_profile_username);
        txType = findViewById(R.id.tx_profile_type);
        txEmail = findViewById(R.id.tx_profile_email);
        txPassword = findViewById(R.id.tx_profile_password);

        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        getUserProfile();
        editModeSwitch(false);
    }

    public void getUserProfile() {
        FirebaseDatabase.getInstance().getReference("/Users").child(uid)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot != null) {

                    Object username = dataSnapshot.child("username").getValue();
                    txUsername.setText(username != null ? username.toString() : "");

                    Object type = dataSnapshot.child("type").getValue();
                    txType.setText(type != null ? type.toString() : "");

                    Object image = dataSnapshot.child("image").getValue();
                    if(image != null)
                        Glide.with(getApplicationContext())
                                .load(Uri.parse((String) image))
                                .into(imageView);
                    else {
                        imageView.setImageResource(R.drawable.user_image);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        txEmail.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());

        oldUsername = txUsername.getText().toString();
        oldType = txType.getText().toString();
        oldEmail = txEmail.getText().toString();
    }

    public void editModeSwitch(boolean value) {
        txUsername.setEnabled(value);
        txType.setEnabled(value);
        txEmail.setEnabled(value);
        txPassword.setEnabled(value);

        findViewById(R.id.bt_profile_edit).setEnabled(!value);
        findViewById(R.id.bt_profile_save).setEnabled(value);

        isEditMode = value;
    }

    public void updateProfile() {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Users").child(uid);

        if(!txPassword.getText().toString().isEmpty())
            FirebaseAuth.getInstance().getCurrentUser().updatePassword(txPassword.getText().toString());
        if(!txEmail.getText().toString().equals(oldEmail))
            FirebaseAuth.getInstance().getCurrentUser().updateEmail(txEmail.getText().toString());
        if(!txUsername.getText().toString().equals(oldUsername))
            dbRef.child("username").setValue(txUsername.getText().toString());
        if(!txType.getText().toString().equals(oldType))
            dbRef.child("type").setValue(txType.getText().toString());
        if(newImage != null)
            uploadImage();

        Toast.makeText(this, R.string.toast_profile_updated, Toast.LENGTH_LONG).show();
    }

    private void discardEditDialog() {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(getString(R.string.project_dialog_title));
        alert.setMessage(getString(R.string.project_dialog_message));
        alert.setButton(Dialog.BUTTON_POSITIVE,getString(R.string.project_dialog_leave),new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alert.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.project_dialog_stay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alert.show();
    }

    private void openGallery() {
        if(isEditMode) {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(gallery, PICK_IMAGE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE && data != null){
            Uri imageUri = data.getData();
            imageView.setImageURI(imageUri);
            imageView.setDrawingCacheEnabled(true);
            imageView.buildDrawingCache();
            newImage = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        }
    }

    public void uploadImage() {
        StorageReference mountainsRef = FirebaseStorage.getInstance().getReference("images/user/" + uid + "jpg");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                if (taskSnapshot.getMetadata() != null) {
                    if (taskSnapshot.getMetadata().getReference() != null) {
                        Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                        result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String imageUrl = uri.toString();
                                Log.w("ProfileEditor", "Upload OK, URL: " + imageUrl);

                                DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Users");
                                dbRef.child(uid).child("image").setValue(imageUrl);
                            }
                        });
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.w("ProjectView", "Upload failed");
            }
        });
    }

}