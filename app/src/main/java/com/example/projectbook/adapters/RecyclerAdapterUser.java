package com.example.projectbook.adapters;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projectbook.R;
import com.example.projectbook.UserView;
import com.example.projectbook.objects.User;

import java.util.ArrayList;

public class RecyclerAdapterUser extends RecyclerView.Adapter<RecyclerAdapterUser.UserItemHolder> {

    private final ArrayList<User> userItems;
    public String ownerKey;

    public RecyclerAdapterUser(ArrayList<User> items, String ownerKey) {
        userItems = items;
        this.ownerKey = ownerKey;
    }

    public RecyclerAdapterUser(ArrayList<User> items) {
        userItems = items;
    }

    @NonNull
    @Override
    public RecyclerAdapterUser.UserItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, parent, false);
        return new UserItemHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterUser.UserItemHolder holder, int position) {
        User user = userItems.get(position);
        if(ownerKey != null)
            holder.bindTodoItem(user, ownerKey);
        else
            holder.bindTodoItem(user);
    }

    @Override
    public int getItemCount() {
        return userItems.size();
    }

    public static class UserItemHolder extends RecyclerView.ViewHolder  implements View.OnClickListener  {
        private final ImageView userImage;
        private final TextView userName;
        private final TextView userType;
        private final TextView userRole;
        private final TextView userRoleLabel;
        private User userItem;
        final View v;

        public UserItemHolder(View v) {
            super(v);
            userImage = v.findViewById(R.id.user_item_image);
            userName = v.findViewById(R.id.tx_user_name);
            userType = v.findViewById(R.id.tx_user_type);
            userRole = v.findViewById(R.id.tx_user_role);
            userRoleLabel = v.findViewById(R.id.label_user_role);
            v.setOnClickListener(this);
            this.v = v;
        }

        @Override
        public void onClick(View v) {
            v.getContext().startActivity((new Intent(v.getContext(), UserView.class)).putExtra("user", userItem));
        }

        public void bindTodoItem(User user) {
            userItem = user;
            userName.setText(user.username);
            userType.setText(user.type);
            userImage.setImageResource(R.drawable.user_image);
            userRoleLabel.setVisibility(View.INVISIBLE);

            if(user.image != null) {
                Glide.with(v.getContext())
                        .load(Uri.parse(user.image))
                        .into(userImage);
            }
        }

        public void bindTodoItem(User user, String ownerKey) {
            userItem = user;
            userName.setText(user.username);
            userType.setText(user.type);
            userImage.setImageResource(R.drawable.user_image);

            if(ownerKey.equals(user.key))
                userRole.setText(R.string.label_owner);

            if(user.image != null) {
                Glide.with(v.getContext())
                        .load(Uri.parse(user.image))
                        .into(userImage);
            }
        }
    }




}
