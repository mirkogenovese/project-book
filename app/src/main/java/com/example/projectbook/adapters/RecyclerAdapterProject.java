package com.example.projectbook.adapters;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projectbook.ProjectView;
import com.example.projectbook.R;
import com.example.projectbook.objects.Project;

import java.util.ArrayList;

public class RecyclerAdapterProject extends RecyclerView.Adapter<RecyclerAdapterProject.ProjectItemHolder> {

    private final ArrayList<Project> projectItems;

    public RecyclerAdapterProject(ArrayList<Project> items) {
        projectItems = items;
    }

    @NonNull
    @Override
    public RecyclerAdapterProject.ProjectItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_project, parent, false);
        return new ProjectItemHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterProject.ProjectItemHolder holder, int position) {
        Project project = projectItems.get(position);
        holder.bindTodoItem(project);
    }

    @Override
    public int getItemCount() {
        return projectItems.size();
    }

    public static class ProjectItemHolder extends RecyclerView.ViewHolder  implements View.OnClickListener  {
        private final ImageView projectImage;
        private final TextView projectName;
        private final TextView projectType;
        private final TextView projectLocation;
        private Project projectItem;
        private final View v;

        public ProjectItemHolder(View v) {
            super(v);
            projectImage = v.findViewById(R.id.project_item_image);
            projectName = v.findViewById(R.id.tx_project_name);
            projectType = v.findViewById(R.id.tx_project_type);
            projectLocation = v.findViewById(R.id.tx_project_location);
            projectLocation.setSelected(true);
            v.setOnClickListener(this);
            this.v = v;
        }

        @Override
        public void onClick(View v) {
            v.getContext().startActivity((new Intent(v.getContext(), ProjectView.class)).putExtra("project", projectItem));
        }

        public void bindTodoItem(Project project) {
            projectItem = project;
            projectName.setText(project.name);
            projectType.setText(project.type);
            projectLocation.setText(project.locationString);
            projectImage.setImageResource(R.drawable.project_image);

            if(project.image != null) {
                Glide.with(v.getContext())
                    .load(Uri.parse(project.image))
                    .into(projectImage);
            }
        }
    }


}
