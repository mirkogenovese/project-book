package com.example.projectbook.adapters;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbook.R;
import com.example.projectbook.objects.Notification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RecyclerAdapterNotification extends RecyclerView.Adapter<RecyclerAdapterNotification.notificationItemHolder> {

    private final ArrayList<Notification> notificationItems;

    public RecyclerAdapterNotification(ArrayList<Notification> items) {
        notificationItems = items;
    }

    @NonNull
    @Override
    public notificationItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        return new notificationItemHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull notificationItemHolder holder, int position) {
        Notification notification = notificationItems.get(position);
        holder.bindTodoItem(notification);
    }

    @Override
    public int getItemCount() {
        return notificationItems.size();
    }

    public static class notificationItemHolder extends RecyclerView.ViewHolder {
        private Notification notificationItem;
        private final View layout;
        private final TextView title;
        private final TextView message;
        private final Button notificationAccept;
        private final Button notificationDecline;
        private String projectName = "";
        private String userName = "";
        private String messageText = "";
        private final View v;

        public notificationItemHolder(View v) {
            super(v);
            layout = v.findViewById(R.id.notification_layout);
            title = v.findViewById(R.id.notification_title);
            message = v.findViewById(R.id.notification_message);
            notificationAccept = v.findViewById(R.id.btn_notification_accept);
            notificationDecline = v.findViewById(R.id.btn_notification_decline);
            this.v = v;
        }

        public void bindTodoItem(final Notification notification) {

            GradientDrawable background = (GradientDrawable) title.getBackground();
            background.setColor(Color.parseColor("#99DC8E"));

            background = (GradientDrawable) notificationDecline.getBackground();
            background.setColor(Color.parseColor("#FFCC3E3E"));

            background = (GradientDrawable) notificationAccept.getBackground();
            background.setColor(Color.CYAN);

            background = (GradientDrawable) layout.getBackground();
            background.setColor(Color.WHITE);

            notificationItem = notification;
            if(notification.type == 1) {
                messageText = v.getContext().getResources().getString(R.string.notification_project_entry_request);
                title.setText(R.string.notification_title_entry_request);
            } else {
                messageText = v.getContext().getResources().getString(R.string.notification_project_invite);
                title.setText(R.string.notification_title_invite);
            }
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("Projects").child(notification.projectKey);
            dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Object value = dataSnapshot.child("name").getValue();
                    projectName = value != null ? value.toString() : "";
                    message.setText(String.format(messageText, userName, projectName));
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    Log.w("RecycleAdapterChat", "Failed to read value.", error.toException());
                }
            });

            dbRef = FirebaseDatabase.getInstance().getReference("Users").child(notification.userKey);
            dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Object value = dataSnapshot.child("username").getValue();
                    userName = value != null ? value.toString() : "";
                    message.setText(String.format(messageText, userName, projectName));
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    Log.w("RecycleAdapterChat", "Failed to read value.", error.toException());
                }
            });

            notificationAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(notification.type == 1) {
                        messageText = v.getContext().getResources().getString(R.string.notification_project_entry_accepted);
                        addParticipantToProject(false);
                        Toast.makeText(view.getContext(), String.format(messageText, userName, projectName), Toast.LENGTH_LONG).show();

                    } else {
                        messageText = v.getContext().getResources().getString(R.string.notification_project_invite_accepted);
                        addParticipantToProject(true);
                        Toast.makeText(view.getContext(), String.format(messageText, projectName), Toast.LENGTH_LONG).show();
                    }
                    removeNotification(notification.key);
                }
            });

            notificationDecline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeNotification(notification.key);
                }
            });
        }

        private void removeNotification(String key) {
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Users").child(uid).child("Notifications");

            dbRef.child(key).removeValue();
        }

        private void addParticipantToProject(boolean isSelf) {
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
            String userKey = isSelf ? FirebaseAuth.getInstance().getCurrentUser().getUid() : notificationItem.userKey;

            dbRef.child("Projects").child(notificationItem.projectKey).child("Participants").child(userKey).setValue(true);
            dbRef.child("Users").child(userKey).child("Projects").child(notificationItem.projectKey).setValue(true);
        }

    }
}

