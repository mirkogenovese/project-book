package com.example.projectbook.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projectbook.ChatView;
import com.example.projectbook.R;
import com.example.projectbook.objects.ChatItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RecyclerAdapterChat extends RecyclerView.Adapter<RecyclerAdapterChat.ChatItemHolder> {

    private final ArrayList<ChatItem> chatItems;

    public RecyclerAdapterChat(ArrayList<ChatItem> items) {
        chatItems = items;
    }

    @NonNull
    @Override
    public ChatItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat, parent, false);
        return new ChatItemHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatItemHolder holder, int position) {
        ChatItem chat = chatItems.get(position);
        holder.bindTodoItem(chat);
    }

    @Override
    public int getItemCount() {
        return chatItems.size();
    }

    public static class ChatItemHolder extends RecyclerView.ViewHolder  implements View.OnClickListener  {
        private final ImageView chatImage;
        private final TextView chatName;
        private final TextView chatLastMessage;
        private final TextView chatNewMessages;
        private ChatItem chatItem;
        private final View v;

        public ChatItemHolder(View v) {
            super(v);
            chatImage = v.findViewById(R.id.chat_item_image);
            chatName = v.findViewById(R.id.tx_chat_name);
            chatLastMessage = v.findViewById(R.id.tx_chat_last_message);
            chatNewMessages = v.findViewById(R.id.tx_chat_new_messages);
            v.setOnClickListener(this);
            this.v = v;

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    hideChatDialog();
                    return true;
                }
            });
        }

        @Override
        public void onClick(View v) {
            chatNewMessages.setVisibility(View.INVISIBLE);
            v.getContext().startActivity((new Intent(v.getContext(), ChatView.class)).putExtra("chat", chatItem));
        }


        public void hideChatDialog() {
            AlertDialog alert = new AlertDialog.Builder(v.getContext()).create();
            alert.setTitle(v.getContext().getString(R.string.dialog_hide_chat_title));
            alert.setMessage(v.getContext().getString(R.string.dialog_hide_chat_message));
            alert.setButton(Dialog.BUTTON_POSITIVE,v.getContext().getString(R.string.dialog_hide_chat_confirm),new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseDatabase.getInstance().getReference("/Users")
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child("Chats")
                            .child(chatItem.key).removeValue();
                }
            });
            alert.setButton(Dialog.BUTTON_NEGATIVE, v.getContext().getString(R.string.dialog_hide_chat_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            alert.show();
        }

        public void bindTodoItem(final ChatItem chat) {
            chatItem = chat;
            chatLastMessage.setText(chat.lastMessageDate);

            if(chat.messageCount > 0) {
                chatNewMessages.setVisibility(View.VISIBLE);
                chatNewMessages.setText("" + chat.messageCount);
            }
            else
                chatNewMessages.setVisibility(View.INVISIBLE);

            DatabaseReference dbRef = null;
            if(chat.groupKey != null)
                dbRef = FirebaseDatabase.getInstance().getReference("Projects").child(chat.groupKey);
            else {
                if(chat.user1Key != null && chat.user2Key != null) {
                    if(chat.user1Key.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                        dbRef = FirebaseDatabase.getInstance().getReference("Users").child(chat.user2Key);
                    else
                        dbRef = FirebaseDatabase.getInstance().getReference("Users").child(chat.user1Key);
                }
            }
            if(dbRef != null) {
                dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Object name;
                        if(chat.groupKey != null)
                            name = dataSnapshot.child("name").getValue();
                         else
                            name = dataSnapshot.child("username").getValue();
                        chat.name = name != null ? name.toString() : "";

                        Object image = dataSnapshot.child("image").getValue();
                        chat.image = image != null ? image.toString() : null;

                        chatName.setText(chat.name);

                        if(chat.groupKey != null)
                            chatImage.setImageResource(R.drawable.project_image);
                        else
                            chatImage.setImageResource(R.drawable.user_image);

                        if(chat.image != null) {
                            Glide.with(v.getContext())
                                    .load(Uri.parse(chat.image))
                                    .into(chatImage);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.w("RecycleAdapterChat", "Failed to read value.", error.toException());
                    }
                });
            }
        }
    }
}
