package com.example.projectbook.adapters;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.projectbook.ProjectInfoFragment;
import com.example.projectbook.ProjectParticipantsFragment;
import com.example.projectbook.R;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final Context mContext;
    final int totalTabs;

    public SectionsPagerAdapter(Context context, FragmentManager fm, int tabCount) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
        totalTabs = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ProjectInfoFragment();
            case 1:
                return new ProjectParticipantsFragment();
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.info_fragment_title);
            case 1:
                return mContext.getString(R.string.participants_fragment_title);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}