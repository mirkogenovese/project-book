package com.example.projectbook;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.projectbook.objects.ChatItem;
import com.example.projectbook.objects.ChatMessage;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import static com.google.firebase.database.FirebaseDatabase.getInstance;

public class ChatView extends AppCompatActivity {

    private FirebaseListAdapter<ChatMessage> adapter;
    String chatKey;
    ChatItem chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        chat = (ChatItem) getIntent().getSerializableExtra("chat");
        chatKey = chat.key;
        getSupportActionBar().setTitle(chat.name);

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText input = findViewById(R.id.input);

                ChatMessage message = new ChatMessage(input.getText().toString(), MainActivity.username);

                DatabaseReference dbRef = getInstance().getReference("/Chats").child(chatKey);
                dbRef.child("lastMessageDate").setValue(message.getTime());

                dbRef = getInstance().getReference("/Chats").child(chatKey).child("Messages");
                dbRef.push().setValue(message);

                input.setText("");
            }
        });

        displayChatMessages();
    }

    public void displayChatMessages() {
        ListView listOfMessages = findViewById(R.id.list_of_messages);

        Query query = getInstance().getReference("/Chats").child(chatKey).child("Messages");
        FirebaseListOptions<ChatMessage> options = new FirebaseListOptions.Builder<ChatMessage>()
                .setQuery(query, ChatMessage.class)
                .setLayout(R.layout.item_message)
                .build();

        adapter = new FirebaseListAdapter<ChatMessage>(options) {
            @Override
            protected void populateView(View v, ChatMessage model, int position) {

                TextView messageText = v.findViewById(R.id.message_text);
                TextView messageUser = v.findViewById(R.id.message_user);
                TextView messageTime = v.findViewById(R.id.message_time);

                messageText.setText(model.getText());
                messageUser.setText(model.getSender());

                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getTime()));
            }};

        listOfMessages.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }


    @Override
    protected void onStop() {
        super.onStop();
        updateReadCount();
        adapter.stopListening();
    }

    public void updateReadCount() {
        FirebaseDatabase.getInstance().getReference("/Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("Chats")
                .child(chatKey).setValue(adapter.getCount());
    }
}