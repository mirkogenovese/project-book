package com.example.projectbook;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbook.adapters.RecyclerAdapterChat;
import com.example.projectbook.objects.ChatItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;

public class ConversationsActivity extends AppCompatActivity {

    private static final String TAG = "ConversationsActivity";
    ArrayList<ChatItem> listChat;
    RecyclerAdapterChat adapter;
    final LinkedList<ValueEventListener> conversationsListListeners = new LinkedList<>();
    final LinkedList<ValueEventListener> conversationsListeners = new LinkedList<>();
    final LinkedList<String> conversationsListenersKey = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_recycler_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle(R.string.conversation_activity_title);

        initializeChatList();
    }

    public void initializeChatList() {

        RecyclerView chatRecycleView = findViewById(R.id.recycler_view_list);
        chatRecycleView.setLayoutManager(new LinearLayoutManager(this));

        listChat = new ArrayList<>();
        adapter = new RecyclerAdapterChat(listChat);
        chatRecycleView.setAdapter(adapter);

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        conversationsListListeners.add(FirebaseDatabase.getInstance().getReference("Users").child(uid).child("Chats").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(int i = 0; i < conversationsListeners.size(); i++)
                    FirebaseDatabase.getInstance().getReference("Chats").child(conversationsListenersKey.get(i)).removeEventListener(conversationsListeners.get(i));
                conversationsListeners.clear();
                conversationsListenersKey.clear();
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        }));

    }

    private void showData(DataSnapshot dataSnapshot) {
        listChat.clear();
        for (DataSnapshot ds : dataSnapshot.getChildren()) {

            Object readCountObject = ds.getValue();
            final int readCount = readCountObject != null ? ((Long)readCountObject).intValue() : 0;

            conversationsListenersKey.add(ds.getKey());
            conversationsListeners.add(FirebaseDatabase.getInstance().getReference("/Chats").child(ds.getKey()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.getValue() != null) {
                        ChatItem chat = new ChatItem();
                        chat.key = snapshot.getKey();

                        Long time = (Long) snapshot.child("lastMessageDate").getValue();
                        chat.lastMessageDate = time != null ? (String) DateFormat.format("dd-MM-yyyy (HH:mm:ss)", time) : "";

                        Object groupKey = snapshot.child("groupKey").getValue();
                        chat.groupKey = groupKey != null ? groupKey.toString() : null;

                        Object user1Key = snapshot.child("user1Key").getValue();
                        chat.user1Key = user1Key != null ? user1Key.toString() : null;

                        Object user2Key = snapshot.child("user2Key").getValue();
                        chat.user2Key = user2Key != null ? user2Key.toString() : null;

                        long count = snapshot.child("Messages").getChildrenCount();
                        chat.messageCount = (int) count - readCount;

                        listChat.remove(chat);
                        listChat.add(chat);

                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            }));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(ValueEventListener l : conversationsListListeners)
            FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Chats").removeEventListener(l);
        for(int i = 0; i < conversationsListeners.size(); i++)
            FirebaseDatabase.getInstance().getReference("Chats").child(conversationsListenersKey.get(i)).removeEventListener(conversationsListeners.get(i));
    }
}