package com.example.projectbook;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.example.projectbook.objects.Project;
import com.example.projectbook.utilities.MarkerClusterRenderer;
import com.example.projectbook.utilities.Utilities;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.clustering.ClusterManager;

public class ProjectLocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private LatLng projectLocation;
    private Marker marker;
    private int mode = 0;
    private ClusterManager<Project> mClusterManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_location);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        projectLocation = getIntent().getParcelableExtra("location");

        String command = getIntent().getStringExtra("command");
        switch (command) {
            case "showLocation":
                mode = 0;
                getSupportActionBar().setTitle(R.string.project_location);
                break;
            case "selectLocation":
                mode = 1;
                getSupportActionBar().setTitle(R.string.set_location);
                break;
            case "selectProject":
                mode = 2;
                getSupportActionBar().setTitle(R.string.find_project);
                break;
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(projectLocation != null) {
                    Intent data = new Intent();
                    data.putExtra("location", projectLocation);
                    data.putExtra("locationString", Utilities.getAddress(projectLocation, getApplicationContext()));
                    setResult(RESULT_OK, data);
                }
                else
                    setResult(RESULT_CANCELED);
                finish();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        setUpMap();
    }

    private void setUpMap() {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }

        mMap.setMyLocationEnabled(true);

        if(projectLocation != null) {
            LatLng projectLatLng = new LatLng(projectLocation.latitude, projectLocation.longitude);
            placeMarkerOnMap(projectLatLng);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(projectLatLng, 12f));
        } else {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location != null) {
                        LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f));
                    }
                }
            });
        }

        if(mode == 1)
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    placeMarkerOnMap(latLng);
                    projectLocation = latLng;
                }
            });
        if(mode == 2) {
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    Project project = (Project) marker.getTag();
                    openProject(project);
                }
            });
            setUpCluster();
        }
    }

    private void placeMarkerOnMap(LatLng location) {
        if(marker != null)
            marker.remove();
        marker = mMap.addMarker(new MarkerOptions()
                .position(location)
                .title(Utilities.getAddress(location, this)));
    }

    private void openProject(Project project) {
        startActivity(new Intent(this, ProjectView.class).putExtra("project", project));
    }

    private void setUpCluster() {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        mClusterManager = new ClusterManager<>(this, mMap);
        mClusterManager.setRenderer(new MarkerClusterRenderer(this, mMap, mClusterManager));
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        addItems();
    }

    private void addItems() {
        for(Project p : SearchProject.listProjects) {
            if(p.longitude != null && p.latitude != null){
                mClusterManager.addItem(p);
            }
        }
    }

}