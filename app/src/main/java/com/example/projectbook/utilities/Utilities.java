package com.example.projectbook.utilities;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

public class Utilities {

    public static String getAddress(LatLng latLng, Context context) {
        Geocoder geocoder = new Geocoder(context);
        List<Address> addresses;
        Address address;
        String addressText = "";
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses.get(0);
                StringBuilder addressTextBuilder = new StringBuilder(address.getAddressLine(0));
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
                    addressTextBuilder.append("\n").append(address.getAddressLine(i));
                addressText = addressTextBuilder.toString();
            }
        } catch (IOException e) {
            Log.e("MapsActivity", e.getLocalizedMessage());
        }
        return addressText;
    }

}
