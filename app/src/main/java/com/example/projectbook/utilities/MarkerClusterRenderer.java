package com.example.projectbook.utilities;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.projectbook.R;
import com.example.projectbook.objects.Project;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class MarkerClusterRenderer  extends DefaultClusterRenderer<Project> {

    private final LayoutInflater layoutInflater;

    public MarkerClusterRenderer(Context context, GoogleMap map, ClusterManager<Project> clusterManager) {
        super(context, map, clusterManager);

        layoutInflater = LayoutInflater.from(context);
        map.setInfoWindowAdapter(clusterManager.getMarkerManager());
        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomMarkerInfoWindowView());
    }

    @Override
    protected void onClusterItemRendered(Project clusterItem, Marker marker) {
        marker.setTag(clusterItem);
    }

    public class CustomMarkerInfoWindowView  implements GoogleMap.InfoWindowAdapter {

        private final View markerItemView;

        public CustomMarkerInfoWindowView() {
            markerItemView = layoutInflater.inflate(R.layout.map_info_project, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            Project project = (Project) marker.getTag();
            ((TextView)markerItemView.findViewById(R.id.tx_map_project_name)).setText(marker.getTitle());
            ((TextView)markerItemView.findViewById(R.id.tx_map_project_type)).setText(project.type);
            ((TextView)markerItemView.findViewById(R.id.tx_map_project_location)).setText(project.locationString);
            ((ImageView)markerItemView.findViewById(R.id.map_project_image)).setImageResource(R.drawable.project_image);

            if(project.image != null) {
                Glide.with(markerItemView.getContext())
                        .load(Uri.parse(project.image))
                        .into(((ImageView)markerItemView.findViewById(R.id.map_project_image)));
            }
            return markerItemView;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }
}
