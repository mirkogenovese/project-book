package com.example.projectbook.objects;

import java.io.Serializable;

public class ChatItem implements Serializable {

    public String key;
    public String name;
    public String lastMessageDate;
    public String groupKey;
    public String user1Key;
    public String user2Key;
    public String image;
    public int messageCount;

    @Override
    public boolean equals(Object v) {
        if (v instanceof ChatItem)
            return ((ChatItem) v).key.equals(key);
        return false;
    }
}
