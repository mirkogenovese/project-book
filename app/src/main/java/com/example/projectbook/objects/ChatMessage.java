package com.example.projectbook.objects;

import java.io.Serializable;
import java.util.Date;

public class ChatMessage implements Serializable {

    private String text;
    private String sender;
    private long time;

    public ChatMessage(String text, String sender) {
        this.text = text;
        this.sender = sender;
        time = new Date().getTime();
    }

    public ChatMessage(){

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}