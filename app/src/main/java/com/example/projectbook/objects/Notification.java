package com.example.projectbook.objects;

public class Notification {

    public String key;
    public int type;
    public String projectKey;
    public String userKey;
}
