package com.example.projectbook.objects;

import java.io.Serializable;

public class User implements Serializable {

    public String key = "";
    public String username = "";
    public String type = "";
    public String image;

    @Override
    public String toString() {
        return username + "\n" + type + "\n" + type;
    }

    @Override
    public boolean equals(Object v) {
        if (v instanceof User)
            return ((User) v).key.equals(key);
        return false;
    }
}