package com.example.projectbook.objects;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Project implements Serializable, ClusterItem {

    public String key = "";
    public String name = "";
    public String ownerKey = "";
    public String type = "";
    public Double latitude;
    public Double longitude;
    public String locationString;
    public String description;
    public String createOn;
    public final ArrayList<String> participants = new ArrayList<>();
    public String chat = "";
    public String image;

    public Project() {
        createOn = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
    }

    @Override
    public String toString() {
        return name + "\n" + type + "\n" + locationString + "\n" + description + "\n" +  createOn;
    }

    public void setLocation(LatLng location) {
        latitude = location.latitude;
        longitude = location.longitude;
    }

    public LatLng getLocation() {
        if(latitude == null || longitude == null)
            return null;
        return new LatLng(latitude, longitude);
    }

    @Override
    public boolean equals(Object v) {
        if (v instanceof Project)
            return ((Project) v).key.equals(key);
        return false;
    }

    @Override
    public LatLng getPosition() {
        return getLocation();
    }

    @Override
    public String getTitle() {
        return name;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
