package com.example.projectbook;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.projectbook.objects.ChatItem;
import com.example.projectbook.objects.Project;
import com.example.projectbook.objects.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserView extends AppCompatActivity {

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);

        Toolbar toolbar = findViewById(R.id.user_view_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 finish();
            }
        });

        user = (User) getIntent().getSerializableExtra("user");

        findViewById(R.id.bt_invite_to_project).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createInviteDialog();
            }
        });
        findViewById(R.id.bt_send_message).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createConversation();
            }
        });

        ((TextView) findViewById(R.id.label_user_username)).setText(user.username);
        ((TextView) findViewById(R.id.label_user_type)).setText(user.type);

        if(user.image != null)
            Glide.with(getApplicationContext())
                    .load(Uri.parse(user.image))
                    .into((ImageView) findViewById(R.id.image_user));
        else {
            ((ImageView) findViewById(R.id.image_user)).setImageResource(R.drawable.user_image);
        }

        getSupportActionBar().setTitle(user.username);

        if(user.key.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
        {
            findViewById(R.id.bt_invite_to_project).setEnabled(false);
            findViewById(R.id.bt_send_message).setEnabled(false);
        }
    }

    public void createInviteDialog() {
        final ArrayList<Integer> selectedItems = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final ArrayList<Project> projects = new ArrayList<>(MainActivity.listProjects);
        String[] projectsNames = new String[projects.size()];
        for(int i = 0; i < projects.size(); i++) {
            projectsNames[i] = projects.get(i).name;
        }
        builder.setTitle(R.string.invite_dialog_title)
                .setMultiChoiceItems(projectsNames, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    selectedItems.add(which);
                                } else if (selectedItems.contains(which)) {
                                    selectedItems.remove(Integer.valueOf(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.invite_dialog_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        ArrayList<Project> selectedProjects = new ArrayList<>();
                        for(int i : selectedItems) {
                            selectedProjects.add(projects.get(i));
                        }
                        inviteToProjects(selectedProjects);
                    }
                })
                .setNegativeButton(R.string.invite_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        builder.show();
    }

    public void inviteToProjects(ArrayList<Project> selectedProjects) {
        for(Project p : selectedProjects) {
            DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
            String key = dbRef.child("Users").child(user.key).child("Notifications").push().getKey();
            dbRef = dbRef.child("Users").child(user.key).child("Notifications").child(key);
            dbRef.child("userKey").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
            dbRef.child("projectKey").setValue(p.key);
            dbRef.child("type").setValue(0);
        }

        Toast.makeText(this, R.string.toast_user_invited, Toast.LENGTH_LONG).show();
    }

    public void createConversation() {

        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("/Chats");
        ChatItem chat = new ChatItem();

        String uid = FirebaseAuth.getInstance().getUid();

        String key;
        if(uid.compareTo(user.key) > 0) {
            key = user.key + "-" + uid;
            chat.user1Key = user.key;
            chat.user2Key = uid;
        }
        else {
            key = uid + "-" + user.key;
            chat.user1Key = uid;
            chat.user2Key = user.key;
        }

        dbRef.child(key).child("user1Key").setValue(chat.user1Key);
        dbRef.child(key).child("user2Key").setValue(chat.user2Key);

        dbRef = FirebaseDatabase.getInstance().getReference("/Users");
        dbRef.child(mFirebaseUser.getUid()).child("Chats").child(key).setValue(0);
        final String userKey = user.key;
        final String chatKey = key;
        dbRef.child(user.key).child("Chats").child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() == null) {
                    Log.w("userView", "chat null");
                    FirebaseDatabase.getInstance().getReference("/Users")
                            .child(userKey).child("Chats").child(chatKey).setValue(0);
                } else
                    Log.w("userView", "chat exist");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        Intent intent = new Intent(this, ConversationsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
