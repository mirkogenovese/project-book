package com.example.projectbook;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbook.adapters.RecyclerAdapterUser;
import com.example.projectbook.objects.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;

public class ProjectParticipantsFragment extends Fragment {

    ArrayList<User> listUsers;
    RecyclerAdapterUser adapter;
    final LinkedList<ValueEventListener> userListListeners = new LinkedList<>();

    public ProjectParticipantsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflated =  inflater.inflate(R.layout.fragment_project_participants, container, false);

        initializeUsersList(inflated);
        return inflated;
    }

    public void initializeUsersList(View view) {

        RecyclerView userList = view.findViewById(R.id.list_participant);
        userList.setLayoutManager(new LinearLayoutManager(view.getContext()));

        listUsers = new ArrayList<>();
        adapter = new RecyclerAdapterUser(listUsers, ProjectView.project.ownerKey);
        userList.setAdapter(adapter);
        userListListeners.add(FirebaseDatabase.getInstance().getReference("Projects").child(ProjectView.project.key).child("Participants").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w("ParticipantsFragment", "Failed to read value.", error.toException());
            }
        }));
    }

    private void showData(DataSnapshot dataSnapshot){
        listUsers.clear();
        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            FirebaseDatabase.getInstance().getReference("Users").child(ds.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.getValue() != null) {
                        User user = new User();
                        user.key = snapshot.getKey();

                        Object username = snapshot.child("username").getValue();
                        user.username = username != null ? username.toString() : "";

                        Object type = snapshot.child("type").getValue();
                        user.type = type != null ? type.toString() : "";

                        Object image = snapshot.child("image").getValue();
                        user.image = image != null ? image.toString() : null;

                        listUsers.remove(user);
                        listUsers.add(user);
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(ValueEventListener l : userListListeners)
            FirebaseDatabase.getInstance().getReference("Projects").child(ProjectView.project.key).child("Participants").removeEventListener(l);
    }

}