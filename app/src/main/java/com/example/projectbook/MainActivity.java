package com.example.projectbook;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbook.adapters.RecyclerAdapterProject;
import com.example.projectbook.objects.Project;
import com.example.projectbook.utilities.Utilities;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static String username = "";
    boolean notifications = false;

    public static ArrayList<Project> listProjects;
    RecyclerAdapterProject adapter;
    final LinkedList<ValueEventListener> projectsListListeners = new LinkedList<>();
    final ArrayList<ValueEventListener> projectsListeners = new ArrayList<>();
    final ArrayList<String> projectsListenersKeys = new ArrayList<String>();
    final LinkedList<ValueEventListener> notificationsListeners = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

       ((Toolbar) findViewById(R.id.toolbar)).setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_notification_yes:
                        openNotificationActivity();
                        return true;
                    case R.id.menu_notification_no:
                        Toast.makeText(getApplicationContext(), R.string.no_notification, Toast.LENGTH_LONG).show();
                        return true;
                }
                return false;
            }
        });

        findViewById(R.id.fab_new_project).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputNameDialog();
            }
        });

        FirebaseAuth.getInstance().addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null)
                    loadLogInView();
                else
                    loadMainActivity();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notification, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.menu_notification_yes).setEnabled(notifications).setVisible(notifications);
        menu.findItem(R.id.menu_notification_no).setEnabled(!notifications).setVisible(!notifications);
        ((TextView)findViewById(R.id.notification_count)).setVisibility(notifications ? View.VISIBLE : View.INVISIBLE);
        return true;
    }

    public void loadMainActivity() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase.getInstance().getReference("/Users").child(user.getUid()).child("username")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        username = dataSnapshot.getValue(String.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,"Error while reading data");
                    }
                });

        loadMyProjects();
        DrawerLayout drawerLayout = findViewById(R.id.main_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ActionBarDrawerToggle t = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);

        drawerLayout.addDrawerListener(t);
        t.syncState();

        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                ((DrawerLayout) findViewById(R.id.main_drawer)).closeDrawers();
                switch(id)
                {
                    case R.id.menu_profile:
                        openProfileEditor();
                        break;
                    case R.id.menu_conversation:
                        openConversationActivity();
                        break;
                    case R.id.menu_search_project:
                        openSearchProject();
                        break;
                    case R.id.menu_search_user:
                        openSearchUser();
                        break;
                    case R.id.menu_logout:
                        onClickLogOut();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });

        getNotifications();
    }

    public void openProfileEditor() {
        startActivity(new Intent(this, ProfileEditor.class));
    }

    public void openSearchProject() {
        startActivity(new Intent(this, SearchProject.class));
    }

    public void openSearchUser() {
        startActivity(new Intent(this, SearchUser.class));
    }

    public void openNotificationActivity() {
        startActivity(new Intent(this, NotificationActivity.class));
    }

    public void openConversationActivity() {
        startActivity(new Intent(this, ConversationsActivity.class));
    }

    private void loadLogInView() {
        Intent intent = new Intent(this, LogInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void onClickLogOut() {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(MainActivity.this, LogInActivity.class));
        finish();
    }

    public void clickAddNewProject(String projectName) {
        startActivity((new Intent(this, ProjectView.class))
                .putExtra("name", projectName));
    }

    private void showInputNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.project_dialog_name);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

        builder.setPositiveButton(R.string.project_dialog_name_create, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(input.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.project_dialog_invalid_name, Toast.LENGTH_LONG).show();
                    return;
                }
                clickAddNewProject(input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.project_dialog_name_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void loadMyProjects() {
        String mUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        RecyclerView projectList = findViewById(R.id.recycler_view_list);
        projectList.setLayoutManager(new LinearLayoutManager(this));

        listProjects = new ArrayList<>();
        adapter = new RecyclerAdapterProject(listProjects);
        projectList.setAdapter(adapter);

        projectsListListeners.add(FirebaseDatabase.getInstance().getReference("Users").child(mUserId).child("Projects").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(int i = 0; i < projectsListeners.size(); i++)
                    FirebaseDatabase.getInstance().getReference("Projects").child(projectsListenersKeys.get(i)).removeEventListener(projectsListeners.get(i));
                projectsListeners.clear();
                projectsListenersKeys.clear();
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        }));
    }

    private void showData(DataSnapshot dataSnapshot){
        listProjects.clear();

        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            projectsListenersKeys.add(ds.getKey());
            projectsListeners.add(FirebaseDatabase.getInstance().getReference("Projects").child(ds.getKey()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.getValue() != null) {
                        Project project = new Project();
                        project.key = snapshot.getKey();

                        Object name = snapshot.child("name").getValue();
                        project.name = name != null ? name.toString() : "";

                        Object type = snapshot.child("type").getValue();
                        project.type = type != null ? type.toString() : "";

                        Object chat = snapshot.child("chat").getValue();
                        project.chat = chat != null ? chat.toString() : "";

                        Object ownerKey = snapshot.child("ownerKey").getValue();
                        project.ownerKey = ownerKey != null ? ownerKey.toString() : "";

                        Object image = snapshot.child("image").getValue();
                        project.image = image != null ? image.toString() : null;

                        Double latitude = (Double) snapshot.child("location").child("latitude").getValue();
                        Double longitude = (Double) snapshot.child("location").child("longitude").getValue();
                        if(latitude != null && longitude != null) {
                            project.latitude = latitude;
                            project.longitude = longitude;
                            project.locationString = Utilities.getAddress(project.getLocation(), getApplicationContext());
                        }

                        Object description = snapshot.child("description").getValue();
                        project.description = description != null ? description.toString() : "";

                        Object createOn = snapshot.child("createOn").getValue();
                        project.createOn = createOn != null ? createOn.toString() : "";

                        for (DataSnapshot part : snapshot.child("Participants").getChildren()) {
                            project.participants.add(part.getKey());
                        }

                        listProjects.remove(project);
                        listProjects.add(project);
                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled (DatabaseError databaseError){
                }
            }));
        }
        adapter.notifyDataSetChanged();
    }

    public void getNotifications() {
        notificationsListeners.add(FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getUid()).child("Notifications")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        int count = (int) dataSnapshot.getChildrenCount();
                        notifications = count > 0;
                        ((TextView)findViewById(R.id.notification_count)).setText("" + count);
                        invalidateOptionsMenu();
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.w("NotificationActivity", "Failed to read value.", error.toException());
                    }
                }));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(ValueEventListener l : projectsListListeners)
            FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Projects").removeEventListener(l);
        for(ValueEventListener l : notificationsListeners)
            FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getUid()).child("Notifications").removeEventListener(l);
        for(int i = 0; i < projectsListeners.size(); i++)
            FirebaseDatabase.getInstance().getReference("Projects").child(projectsListenersKeys.get(i)).removeEventListener(projectsListeners.get(i));
    }}