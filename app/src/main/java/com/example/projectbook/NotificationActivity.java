package com.example.projectbook;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbook.adapters.RecyclerAdapterNotification;
import com.example.projectbook.objects.Notification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;

public class NotificationActivity extends AppCompatActivity {

    public static ArrayList<Notification> listNotifications;
    final LinkedList<ValueEventListener>notificationsListeners = new LinkedList<>();
    RecyclerAdapterNotification adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_recycler_view);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle(R.string.notification_title);
        initializeNotificationsList();
    }

    public void initializeNotificationsList() {

        RecyclerView NotificationRecycleView = findViewById(R.id.recycler_view_list);
        NotificationRecycleView.setLayoutManager(new LinearLayoutManager(this));

        listNotifications = new ArrayList<>();
        adapter = new RecyclerAdapterNotification(listNotifications);
        NotificationRecycleView.setAdapter(adapter);

        notificationsListeners.add(FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getUid()).child("Notifications")
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w("NotificationActivity", "Failed to read value.", error.toException());
            }
        }));
    }

    private void showData(DataSnapshot dataSnapshot){
        listNotifications.clear();
        for(DataSnapshot ds : dataSnapshot.getChildren()){

            Notification notification = new Notification();
            notification.key = ds.getKey();

            Object type = ds.child("type").getValue();
            if(type == null) continue;
            notification.type = ((Long) type).intValue();

            Object groupKey = ds.child("projectKey").getValue();
            if(groupKey == null) continue;
            notification.projectKey = groupKey.toString();

            Object userKey = ds.child("userKey").getValue();
            if(userKey == null) continue;
            notification.userKey = userKey.toString();

            listNotifications.add(notification);
            adapter.notifyDataSetChanged();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(ValueEventListener l : notificationsListeners)
            FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getUid()).child("Notifications").removeEventListener(l);
    }
}